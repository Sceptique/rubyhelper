#encoding: utf-8
require 'minitest/autorun'
require_relative '../lib/rubyhelper'

class HashHelperTest < Minitest::Test

  def test_except
    assert_equal({ :data => :a, :datb => :b },
                 {
      :datx => :x,
      :data => :a,
      :datb => :b,
      :datc => :c,
    }.except!(:datx, :datc))
  end

  def test_rekeys
    assert_equal({new1: 1, new2: 42, stage: 1},
                 {
      old1: 1,
      old2: 42,
      stage: 1,
    }.rekeys(old1: :new1, old2: :new2))
  end

end
